#!/bin/sh

set -e

cd /var/www/html

# Manually download collectives app if `$APP_SOURCE_URL` is set
if [ "$APP_SOURCE_URL" ]; then
    sudo -u www-data wget -O /tmp/collectives.tar.gz "$APP_SOURCE_URL"
    sudo -u www-data tar -C /var/www/html/apps/ -xzf /tmp/collectives.tar.gz
fi

sudo -u www-data /var/www/html/occ app:enable --force collectives
sudo -u www-data /var/www/html/occ collectives:create SearchTest --owner=bob
sudo -u www-data /var/www/html/occ collectives:index
