#!/bin/sh

set -e
set -u

cd /var/www/html

/usr/local/bin/install-collectives.sh

. /etc/apache2/envvars

apache2-foreground
