ARG PHP_MAJOR_VERSION
FROM php:${PHP_MAJOR_VERSION}-apache

# Need to repeat this to also have available after the FROM statement
ARG PHP_MAJOR_VERSION

# RUN echo "deb https://deb.debian.org/debian/ buster-backports main" >/etc/apt/sources.list.d/backports.list
RUN DEBIAN_FRONTEND=noninteractive apt-get update && \
    apt-get install -y --no-install-recommends \
        curl \
        gettext \
        git \
        gpg \
        iproute2 \
        libcurl4-gnutls-dev \
        libfreetype6-dev \
        libicu-dev \
        libjpeg-dev \
        libmagickcore-6.q16-3-extra \
        libmagickwand-dev \
        libmcrypt-dev \
        libpcre3-dev \
        libpng-dev \
        libpq-dev \
        libxml2-dev \
        libzip-dev \
        sudo \
        unzip \
        wget && \
    DEBIAN_FRONTEND=noninteractive apt-get -y autoremove && \
    DEBIAN_FRONTEND=noninteractive apt-get clean && \
    rm -rf /var/lib/apt/lists/*

RUN curl -s https://deb.nodesource.com/gpgkey/nodesource.gpg.key | gpg --dearmor >/usr/share/keyrings/nodesource.gpg
RUN echo "deb [signed-by=/usr/share/keyrings/nodesource.gpg] https://deb.nodesource.com/node_16.x buster main" >/etc/apt/sources.list.d/nodesource.list
RUN DEBIAN_FRONTEND=noninteractive apt-get update && \
    apt-get install -y --no-install-recommends nodejs && \
    DEBIAN_FRONTEND=noninteractive apt-get -y autoremove && \
    DEBIAN_FRONTEND=noninteractive apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# Stick to NPM 8
RUN npm i -g npm@8

RUN if [ "${PHP_MAJOR_VERSION}" = "7.3" ]; then \
    debMultiarch="$(dpkg-architecture --query DEB_BUILD_MULTIARCH)"; \
    docker-php-ext-configure gd --with-freetype-dir=/usr --with-png-dir=/usr --with-jpeg-dir=/usr; \
    else \
    debMultiarch="$(dpkg-architecture --query DEB_BUILD_MULTIARCH)"; \
    docker-php-ext-configure gd --with-freetype --with-jpeg; \
    fi

RUN debMultiarch="$(dpkg-architecture --query DEB_BUILD_MULTIARCH)"; \
    docker-php-ext-install \
        exif \
        gd \
        intl \
        opcache \
        pcntl \
        pdo_mysql \
        pdo_pgsql \
        zip

RUN if [ "${PHP_MAJOR_VERSION}" != "8.1" ]; then \
    pecl install mcrypt; \
    fi

RUN pecl install imagick; \
    docker-php-ext-enable imagick

# configure PHP
RUN { \
        echo 'opcache.enable=1'; \
        echo 'opcache.enable_cli=1'; \
        echo 'opcache.interned_strings_buffer=8'; \
        echo 'opcache.max_accelerated_files=10000'; \
        echo 'opcache.memory_consumption=128'; \
        echo 'opcache.save_comments=1'; \
        echo 'opcache.revalidate_freq=1'; \
    } > /usr/local/etc/php/conf.d/opcache-recommended.ini; \
    echo 'memory_limit=512M' > /usr/local/etc/php/conf.d/memory-limit.ini

RUN echo "ServerName localhost" >> /etc/apache2/apache2.conf
RUN a2enmod rewrite

# Prepare webroot
ENV WEBROOT /var/www/html
WORKDIR /var/www
RUN rmdir /var/www/html && \
    chown www-data:www-data /var/www

# Docker will rebuild from here if NC_BRANCH or LAST_SERVER_COMMIT have changed
ARG NC_BRANCH
ARG LAST_SERVER_COMMIT

# Install Nextcloud
RUN sudo -u www-data git clone --branch ${NC_BRANCH} --depth 1 --shallow-submodules \
        https://github.com/nextcloud/server.git /var/www/html && \
    cd /var/www/html && \
    sudo -u www-data git submodule update --init --depth 1 && \
    sudo -u www-data /var/www/html/occ maintenance:install \
        --admin-user=admin --admin-pass=admin

WORKDIR /var/www/html

# Manually checkout extra apps
ARG CLONE_APPS="text viewer circles contacts"
RUN for app in $CLONE_APPS; do \
    if [ "$app" = "contacts" ]; then \
        if [ "$NC_BRANCH" = "stable22" ] || [ "$NC_BRANCH" = "stable23" ] || [ "$NC_BRANCH" = "stable24" ]; then \
          NC_BRANCH=stable4.2; \
        else \
          NC_BRANCH=main; \
        fi; \
    fi; \
    sudo -u www-data git clone --branch ${NC_BRANCH} --depth 1 \
        https://github.com/nextcloud/$app.git /var/www/html/apps/$app; \
    if [ "$app" = "contacts" ]; then \
        cd /var/www/html/apps/$app && \
        sudo -u www-data npm ci && \
        sudo -u www-data npm run build; \
    fi; \
    done

# Enable apps we need
ARG ENABLE_APPS="text circles contacts"
RUN for app in $ENABLE_APPS; do \
    sudo -u www-data /var/www/html/occ app:enable $app --force; \
    done

# Create Nextcloud users
RUN for user in alice bob jane john; do \
    sudo -u www-data OC_PASS="$user" /var/www/html/occ user:add \
        --password-from-env "$user"; \
    done

# Create Bobs Group and add bob and jane
RUN sudo -u www-data /var/www/html/occ group:add "Bobs Group"
RUN for user in bob jane ; do \
    sudo -u www-data /var/www/html/occ group:adduser "Bobs Group" "$user"; \
    done

# Configure Nextcloud
RUN NC_TRUSTED_DOMAIN_IDX=1 && \
    for domain in localhost nextcloud.local; do \
    sudo -u www-data /var/www/html/occ config:system:set trusted_domains \
        "$NC_TRUSTED_DOMAIN_IDX" --value="$domain"; \
    NC_TRUSTED_DOMAIN_IDX="$(($NC_TRUSTED_DOMAIN_IDX+1))"; \
    done && \
    sudo -u www-data /var/www/html/occ config:system:set \
        --value 'http://nextcloud.local' -- overwrite.cli.url && \
    sudo -u www-data /var/www/html/occ config:system:set \
        --type bool --value true -- allow_local_remote_servers; \
    sudo -u www-data /var/www/html/occ config:system:set \
        loglevel --value '0'; \
    sudo -u www-data /var/www/html/occ config:system:set \
        --type bool --value true -- debug

# Configure circles app
RUN for set in allow_linked_groups allow_non_ssl_links local_is_non_ssl ; do \
    sudo -u www-data /var/www/html/occ config:app:set \
        --value 1 -- circles $set; \
    done

ADD scripts/install-collectives.sh /usr/local/bin/
ADD scripts/run.sh /usr/local/bin/
